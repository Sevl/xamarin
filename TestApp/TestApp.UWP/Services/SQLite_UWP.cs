﻿using System.IO;
using Windows.Storage;
using TestApp.Interfaces;
using TestApp.UWP.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_UWP))]
namespace TestApp.UWP.Services
{
    public class SQLite_UWP : ISQLite
    {
        public SQLite_UWP() { }
        public string GetDatabasePath(string sqliteFilename)
        {
            // для доступа к файлам используем API Windows.Storage
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);
            return path;
        }
    }
}
