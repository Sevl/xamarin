﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using TestApp.Models;
using TestApp.UWP.Services;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace TestApp.UWP.Services
{
    public class CustomButtonRenderer : ViewRenderer<CustomButton, Border>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<CustomButton> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                Border buttonBorder = new Border
                {
                    CornerRadius = new CornerRadius(25),
                    Padding = new Thickness(4),
                    Background = new SolidColorBrush(Windows.UI.Colors.LightBlue),
                    BorderBrush = new SolidColorBrush(Windows.UI.Colors.CadetBlue),
                    BorderThickness = new Thickness(2)
                };
                ContentPresenter contentPresenter = new ContentPresenter
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center
                };
                buttonBorder.Child = contentPresenter;
                buttonBorder.Tapped += (o, args) => { ((Xamarin.Forms.IButtonController)Element).SendClicked(); };

                SetNativeControl(buttonBorder);
                if (e.NewElement != null)
                {
                    SetText();
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == HeaderView.TextProperty.PropertyName)
            {
                SetText();
            }
        }

        private void SetText()
        {
            ContentPresenter contentPresenter = Control.Child as ContentPresenter;
            contentPresenter.Content = Element.Text;
        }
    }
}
