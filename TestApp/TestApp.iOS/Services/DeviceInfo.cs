﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using TestApp.Interfaces;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(TestApp.iOS.Services.DeviceInfo))]
namespace TestApp.iOS.Services
{
    public class DeviceInfo : IDeviceInfo
    {
        public string GetInfo()
        {
            UIDevice device = new UIDevice();
            return $"{device.SystemName} {device.SystemVersion}";
        }
    }
}