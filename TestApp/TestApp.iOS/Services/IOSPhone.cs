﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using TestApp.Interfaces;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(TestApp.iOS.Services.IOSPhone))]
namespace TestApp.iOS.Services
{
    public class IOSPhone : IPhone
    {
        public Task Call(string phoneNumber)
        {
            var nsurl = new NSUrl(new Uri($"tel:{phoneNumber}").AbsoluteUri);
            if (!string.IsNullOrWhiteSpace(phoneNumber) &&
                UIApplication.SharedApplication.CanOpenUrl(nsurl))
            {
                UIApplication.SharedApplication.OpenUrl(nsurl);
            }
            return Task.FromResult(true);
        }
    }
}