﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using TestApp.Interfaces;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(TestApp.iOS.Services.Localize))]
namespace TestApp.iOS.Services
{
    public class Localize : ILocalize
    {
        public System.Globalization.CultureInfo GetCurrentCultureInfo()
        {
            var netLanguage = "en";
            var prefLanguage = "en-US";
            if (NSLocale.PreferredLanguages.Length > 0)
            {
                var pref = NSLocale.PreferredLanguages[0];
                netLanguage = pref.Replace("_", "-"); // заменяет pt_BR на pt-BR
            }
            System.Globalization.CultureInfo ci = null;
            try
            {
                ci = new System.Globalization.CultureInfo(netLanguage);
            }
            catch
            {
                ci = new System.Globalization.CultureInfo(prefLanguage);
            }
            return ci;
        }
    }
}