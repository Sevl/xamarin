﻿using Android.OS;
using TestApp.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(TestApp.Droid.Services.DeviceInfo))]
namespace TestApp.Droid.Services
{
    class DeviceInfo : IDeviceInfo
    {
        public string GetInfo()
        {
            return $"Android {Build.VERSION.Release.ToString()}";
        }
    }
}