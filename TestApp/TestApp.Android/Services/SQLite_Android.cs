﻿using System;
using System.IO;
using Android.Content;
using TestApp.Droid.Services;
using TestApp.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_Android))]
namespace TestApp.Droid.Services
{
    public class SQLite_Android : ISQLite
    {
        public SQLite_Android() { }
        public string GetDatabasePath(string sqliteFilename)
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, sqliteFilename);
            return path;
        }
    }
}