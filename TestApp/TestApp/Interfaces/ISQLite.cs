﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Interfaces
{
    public interface ISQLite
    {
        string GetDatabasePath(string filename);
    }
}
