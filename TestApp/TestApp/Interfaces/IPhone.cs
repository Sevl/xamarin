﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Interfaces
{
    public interface IPhone
    {
        Task Call(string phoneNumber);
    }
}
