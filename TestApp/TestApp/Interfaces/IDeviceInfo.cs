﻿namespace TestApp.Interfaces
{
    public interface IDeviceInfo
    {
        string GetInfo();
    }
}
