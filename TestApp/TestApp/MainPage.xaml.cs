﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Messaging;
using TestApp.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Color = System.Drawing.Color;

namespace TestApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            BackgroundColor = Color.DarkViolet;
            InitializeComponent();
        }
    }
}